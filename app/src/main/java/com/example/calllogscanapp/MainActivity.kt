package com.example.calllogscanapp

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_main.*
import android.app.ActivityManager
import android.content.Context

import android.util.Log
import android.content.Intent





class MainActivity : AppCompatActivity() {

    var mServiceIntent: Intent? = null
    private var mYourService: ScannerService? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


        startBtn.setOnClickListener {
            if(!isMyServiceRunning(ScannerService::class.java))
            {
                mServiceIntent = Intent(this,ScannerService::class.java).apply {
                    putExtra("Kill",false)
                }

                startService(mServiceIntent)
            }
        }

        stopBtn.setOnClickListener {
            isMyServiceRunning(ScannerService::class.java)
            mServiceIntent = Intent(this,ScannerService::class.java).apply {
                putExtra("Kill",true)
            }
            //startService(mServiceIntent)
            stopService(mServiceIntent)
        }
    }


    private fun isMyServiceRunning(serviceClass: Class<*>): Boolean {
        val manager = getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager
        for (service in manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.name == service.service.className) {
                Log.i("Service status", "Running")
                return true
            }
        }
        Log.i("Service status", "Not running")
        return false
    }
}
