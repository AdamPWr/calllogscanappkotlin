package com.example.calllogscanapp

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.telephony.PhoneStateListener
import android.system.Os.listen
import androidx.core.content.ContextCompat.getSystemService
import android.telephony.TelephonyManager
import android.system.Os.listen
import android.util.Log


class PhonestateReceiver : BroadcastReceiver() {

    var telephony: TelephonyManager? = null



    override fun onReceive(context: Context, intent: Intent) {

        Log.d("debug", "CATCHED")

        val phoneListener = MyPhoneStateListener()
        telephony = context
            .getSystemService(Context.TELEPHONY_SERVICE) as TelephonyManager

        telephony?.listen(phoneListener, PhoneStateListener.LISTEN_CALL_STATE)
    }

    fun onDestroy() {
        telephony?.listen(null, PhoneStateListener.LISTEN_NONE)
    }
}
