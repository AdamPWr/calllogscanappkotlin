package com.example.calllogscanapp

import android.telephony.TelephonyManager
import android.telephony.PhoneStateListener
import android.util.Log


class MyPhoneStateListener : PhoneStateListener() {

    override fun onCallStateChanged(state: Int, incomingNumber: String) {
        Log.d("DEBUG", "PhoneState listner debug catch")
        when (state) {
            TelephonyManager.CALL_STATE_IDLE -> {
                Log.d("DEBUG", "IDLE")
                phoneRinging = false
            }
            TelephonyManager.CALL_STATE_OFFHOOK -> {
                Log.d("DEBUG", "OFFHOOK")
                phoneRinging = false
            }
            TelephonyManager.CALL_STATE_RINGING -> {
                Log.d("DEBUG", "RINGING")
                phoneRinging = true
            }
        }
    }

    companion object {

        var phoneRinging: Boolean? = false
    }

}