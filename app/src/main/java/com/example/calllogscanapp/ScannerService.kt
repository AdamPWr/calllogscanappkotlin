package com.example.calllogscanapp

import android.app.Notification
import android.app.Service
import android.content.Intent
import android.os.IBinder
import android.util.Log
import android.app.NotificationManager
import androidx.core.app.NotificationCompat
import android.content.Context.NOTIFICATION_SERVICE
import androidx.core.content.ContextCompat.getSystemService
import android.app.NotificationChannel
import android.content.Context
import android.graphics.Color
import android.os.Build
import androidx.annotation.RequiresApi
import android.icu.lang.UCharacter.GraphemeClusterBreak.T
import androidx.core.content.ContextCompat.getSystemService
import android.icu.lang.UCharacter.GraphemeClusterBreak.T
import androidx.core.content.ContextCompat.getSystemService
import android.icu.lang.UCharacter.GraphemeClusterBreak.T
import android.R.string.cancel
import androidx.core.content.ContextCompat.getSystemService
import android.icu.lang.UCharacter.GraphemeClusterBreak.T
import java.util.*
import android.telephony.PhoneStateListener
import android.system.Os.listen
import android.telephony.TelephonyManager
import androidx.core.content.ContextCompat.getSystemService
import android.icu.lang.UCharacter.GraphemeClusterBreak.T




class ScannerService : Service() {

    val TAG = "DEBUGs"
    var shouldBeTerminated = false
    var running = false

    var phineStateRec: PhonestateReceiver? = null //= com.example.calllogscanapp.PhonestateReceiver()
    var phoneListener : PhoneStateListener? = null

    init {
        Log.d(TAG,"service created")
    }

    override fun onBind(intent: Intent): IBinder? {
        TODO("Return the communication channel to the service.")
        return null
    }

    override fun onCreate() {
        super.onCreate()
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.O)
            startMyOwnForeground()
        else
            startForeground(1, Notification())

    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        super.onStartCommand(intent, flags, startId)

        if (intent != null) {
            shouldBeTerminated = intent.getBooleanExtra("Kill",false)
            Log.d("ONSTART",shouldBeTerminated.toString())
        }
        if(!running)
        {
            workingThread.start()
            //phineStateRec  = PhonestateReceiver()
            if (phoneListener == null) {
               var tm =
                    applicationContext.getSystemService(Context.TELEPHONY_SERVICE) as TelephonyManager
                phoneListener = PhoneStateListener()
                tm.listen(phoneListener, PhoneStateListener.LISTEN_CALL_STATE)
            }
            running = true
        }

        return START_STICKY
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private fun startMyOwnForeground() {
        val NOTIFICATION_CHANNEL_ID = "example.permanence"
        val channelName = "Background Service"
        val chan = NotificationChannel(
            NOTIFICATION_CHANNEL_ID,
            channelName,
            NotificationManager.IMPORTANCE_NONE
        )
        chan.lightColor = Color.BLUE
        chan.lockscreenVisibility = Notification.VISIBILITY_PRIVATE

        val manager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        manager.createNotificationChannel(chan)

        val notificationBuilder = NotificationCompat.Builder(this, NOTIFICATION_CHANNEL_ID)
        val notification = notificationBuilder.setOngoing(true)
            .setContentTitle("App is running in background")
            .setPriority(NotificationManager.IMPORTANCE_MIN)
            .setCategory(Notification.CATEGORY_SERVICE)
            .build()
        startForeground(2, notification)
    }




    var workingThread : Thread  = Thread({

        try {
            mainTask()
        }
        catch (e : InterruptedException)
        {

        }
    })

    fun mainTask()
    {
        Log.d("DEBUGs", "TaskStarted ....")
        while (true)
        {
            Thread.sleep(1000)
            Log.d("DEBUGs", "RUnning ....")
        }
    }
    override fun onDestroy() {
        super.onDestroy()

        //TODO end task
        Log.d("shouldBeTermonDestroy",shouldBeTerminated.toString())

        Log.d(TAG,shouldBeTerminated.toString())
        workingThread.interrupt()
        phineStateRec = null
        if(shouldBeTerminated)
        {
            val broadcastIntent = Intent()
            broadcastIntent.action = "restartservice"
            broadcastIntent.setClass(this, RestarterBroadcatsReceiver::class.java)
            this.sendBroadcast(broadcastIntent)
        }

    }

}
